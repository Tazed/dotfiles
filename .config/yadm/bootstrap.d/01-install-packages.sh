#! /usr/bin/env bash

function prefix {
  awk -v prefix="[$_PREFIX]" '{ print prefix, $0 }'
}
function print {
  echo "$@" | prefix
}
_PREFIX="packages"

function install_apt_package {
  COMMAND=$1
  PACKAGE_NAME=$2
  EXEC_PATH=$(command -v "$COMMAND")
  if [ ! -x "$EXEC_PATH" ]; then
    echo "Installing apt package $PACKAGE_NAME"
    sudo apt-get install -qqy "$PACKAGE_NAME"
  else 
    echo "Command $COMMAND was found"
  fi
} 

function install_pip_package {
  PACKAGE_NAME=$1

  if ! pip show -q "$PACKAGE_NAME"; then 
    echo "Installing pip package $PACKAGE_NAME"
    pip install "$PACKAGE_NAME"
  fi
}

###
# Install apt packagages
###
print "Updating apt package index"
sudo apt-get update -qqq

install_apt_package zsh zsh | prefix
install_apt_package pip python3-pip | prefix
install_apt_package shellcheck shellcheck | prefix
install_apt_package vim vim | prefix
install_apt_package tmux tmux | prefix
install_apt_package fzf fzf | prefix

###
# Install pip packagages
###

install_pip_package vim-vint | prefix
