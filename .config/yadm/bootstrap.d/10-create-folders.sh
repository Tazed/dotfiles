#! /usr/bin/env zsh

function prefix {
  awk -v prefix="[$_PREFIX]" '{ print prefix, $0 }'
}
function print {
  echo "$@" | prefix
}
_PREFIX="folders"

print "Create folders"

mkdir -vp $XDG_CONFIG_HOME | prefix
mkdir -vp $XDG_DATA_HOME | prefix
mkdir -vp $XDG_STATE_HOME | prefix
