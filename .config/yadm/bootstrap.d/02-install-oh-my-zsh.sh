#! /usr/bin/env zsh

function prefix {
  awk -v prefix="[$_PREFIX]" '{ print prefix, $0 }'
}
function print {
  echo "$@" | prefix
}
_PREFIX="oh-my-zsh"

function update_zsh_plugin {
  ZSH_CUSTOM=${ZSH_CUSTOM:-${HOME}/.config/oh-my-zsh_custom}
  PLUGIN_URL=$1
  PLUGIN_NAME=$2
  PLUGIN_PATH="$ZSH_CUSTOM/plugins/$PLUGIN_NAME"
  if [ ! -d "$PLUGIN_PATH" ]; then
    echo "Installing plugin ${PLUGIN_NAME}"
    git clone "$PLUGIN_URL" "$PLUGIN_PATH" 2>&1
  else
    echo "Updating plugin $PLUGIN_NAME"
    git -C "$PLUGIN_PATH" pull --no-rebase 2>&1
  fi
}


if [ ! -d ~/.config/oh-my-zsh ]; then
  print "Installing oh-my-zsh"
  git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.config/oh-my-zsh 2>&1 | prefix
else
  print "oh-my-zsh already installed"
fi

update_zsh_plugin https://github.com/zsh-users/zsh-syntax-highlighting.git zsh-syntax-highlighting 2>&1 | prefix
update_zsh_plugin https://github.com/MichaelAquilina/zsh-you-should-use.git you-should-use 2>&1 | prefix
update_zsh_plugin https://github.com/zsh-users/zsh-completions zsh-completions 2>&1 | prefix
