#! /usr/bin/env zsh

function prefix {
  awk -v prefix="[$_PREFIX]" '{ print prefix, $0 }'
}
function print {
  echo "$@" | prefix
}
_PREFIX="vim"

function update_vim_plugin {
  VIM_FOLDER="$HOME/.config/vim"
  PLUGIN_URL=$1
  PLUGIN_NAME=$2
  PLUGIN_PATH="$VIM_FOLDER/bundle/$PLUGIN_NAME"
  if [ ! -d "$PLUGIN_PATH" ]; then
    echo "Installing plugin ${PLUGIN_NAME}"
    git clone "$PLUGIN_URL" "$PLUGIN_PATH" 2>&1
  else
    echo "Updating plugin $PLUGIN_NAME"
    git -C "$PLUGIN_PATH" pull --no-rebase 2>&1
  fi
}

print "Downloading newest pathogen release"
curl -LSso $HOME/.config/vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim | prefix

update_vim_plugin https://github.com/vim-airline/vim-airline airline | prefix
update_vim_plugin https://github.com/preservim/nerdtree nerdtree | prefix
update_vim_plugin https://github.com/airblade/vim-gitgutter gitgutter | prefix
update_vim_plugin https://github.com/tpope/vim-fugitive fugitive | prefix
update_vim_plugin https://github.com/voldikss/vim-floaterm floaterm | prefix
update_vim_plugin https://github.com/vim-syntastic/syntastic.git syntastic | prefix
