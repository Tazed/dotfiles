#! /usr/bin/env zsh

function prefix {
  awk -v prefix="[$_PREFIX]" '{ print prefix, $0 }'
}
function print {
  echo "$@" | prefix
}
_PREFIX="tmux"

TPM_PATH=$XDG_CONFIG_HOME/tmux/plugins/tpm

function update_tpm {
  TPM_URL=https://github.com/tmux-plugins/tpm.git
  if [ ! -d "$TPM_PATH" ]; then
    echo "Installing tpm"
    git clone "$TPM_URL" "$TPM_PATH" 2>&1
  else
    echo "Updating tpm"
    git -C "$TPM_PATH" pull --no-rebase 2>&1
  fi
}

update_tpm | prefix

print "Installing tpm plugins"
"$TPM_PATH/bin/install_plugins" 2>&1 | prefix
