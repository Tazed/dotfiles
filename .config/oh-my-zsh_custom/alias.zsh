# LS groups folders at the top
alias ls='ls --g --color=tty'
alias lsa='ls --g -lah'
alias l='ls --g -lah'
alias ll='ls --g -lh'
alias la='ls --g -lAh'

alias du='du -h'

alias zshsrc='source "$HOME/.zshenv" "$XDG_CONFIG_HOME/zsh/.zshrc"'
